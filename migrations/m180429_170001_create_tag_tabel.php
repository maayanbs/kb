<?php

use yii\db\Migration;

/**
 * Class m180429_170001_create_tag_tabel
 */
class m180429_170001_create_tag_tabel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    
$this->createTable('tag', [
            'id' => $this->primaryKey(),
            'frequency'=>$this->integer(),
            'name' => $this->string()
]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180429_170001_create_tag_tabel cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180429_170001_create_tag_tabel cannot be reverted.\n";

        return false;
    }
    */
}
