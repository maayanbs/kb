<?php

use yii\db\Migration;

/**
 * Class m180429_170147_article_tag_assn
 */
class m180429_170147_article_tag_assn extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$this->createTable('article_tag_assn', [
            'article_id' => $this->integer(),
            'tag_id'=>$this->integer(),
            'PRIMARY KEY(article_id, tag_id)'
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180429_170147_article_tag_assn cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180429_170147_article_tag_assn cannot be reverted.\n";

        return false;
    }
    */
}
