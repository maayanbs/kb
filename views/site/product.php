<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\models\Product;
use yii\grid\GridView;

$this->title = 'Product';
$this->params['breadcrumbs'][] = $this->title;


$query = Product::find();

$provider = new ActiveDataProvider([
    'query' => $query,


]);


/*
$provider = new ArrayDataProvider([
   'allModels' => $model->getAll()
]);
*/

?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    
    GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            'id',
            'title',
            'body',
        ]
    ])
    
    
    ?> 


    <code><?= __FILE__ ?></code>
</div>